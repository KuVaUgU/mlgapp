package com.kuva.android.mlgapp.activities.login;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.kuva.android.mlgapp.R;
import com.kuva.android.mlgapp.presenters.login.LoginPresenter;
import com.kuva.android.mlgapp.presenters.login.LoginPresenterImpl;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends Activity implements LoginView
{

    @Bind (R.id.username_edit_text) EditText usernameEditText;
    @Bind (R.id.password_edit_text) EditText passwordEditText;
    @Bind (R.id.login_button) Button loginButton;

    @Bind (R.id.login_progress_bar) ProgressBar progressBar;

    private LoginPresenter loginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);

        loginPresenter = new LoginPresenterImpl(this);
    }

    @Override
    public void showProgressBar()
    {
        loginButton.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar()
    {
        loginButton.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void setUsernameEmptyError()
    {
        usernameEditText.setError(getString(R.string.username_empty_error));
    }

    @Override
    public void setPasswordEmptyError()
    {
        passwordEditText.setError(getString(R.string.password_empty_error));
    }

    @Override
    public void setLoginFailedError()
    {
        Toast.makeText(this, R.string.login_failed, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void loginSuccess()
    {
        Toast.makeText(this, "Login Succeses", Toast.LENGTH_LONG).show();
    }

    @OnClick (R.id.login_button)
    void login() {
        loginPresenter.login(usernameEditText.getText().toString(), passwordEditText.getText().toString());
    }
}
