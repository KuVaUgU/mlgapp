package com.kuva.android.mlgapp.activities.login;

/**
 * Created by KuVa on 30.03.2016.
 */
public interface LoginView
{
    void showProgressBar();

    void hideProgressBar();

    void setUsernameEmptyError();

    void setPasswordEmptyError();

    void setLoginFailedError();

    void loginSuccess();
}
