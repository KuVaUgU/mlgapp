package com.kuva.android.mlgapp.presenters.login;

/**
 * Created by KuVa on 30.03.2016.
 */
public interface LoginPresenter
{
    void login(String username, String password);

    void onDestroy();
}
