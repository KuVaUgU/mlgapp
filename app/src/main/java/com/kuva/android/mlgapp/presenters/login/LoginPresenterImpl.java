package com.kuva.android.mlgapp.presenters.login;

import com.kuva.android.mlgapp.activities.login.LoginView;
import com.kuva.android.usecases.LoginUseCase;
import com.kuva.android.usecases.LoginUseCaseImpl;

/**
 * Created by KuVa on 30.03.2016.
 */
public class LoginPresenterImpl implements LoginPresenter, LoginUseCase.OnLoginFinishedListener
{
    private LoginView loginView;
    private LoginUseCase loginUseCase;

    public LoginPresenterImpl(LoginView loginView)
    {
        this.loginView = loginView;
        this.loginUseCase = new LoginUseCaseImpl();
    }

    @Override
    public void login(String username, String password) {
        if (loginView != null) {
            boolean error = false;
            if (username.isEmpty()) {
                loginView.setUsernameEmptyError();
                error = true;
            }

            if (password.isEmpty()) {
                loginView.setPasswordEmptyError();
                error = true;
            }

            if (!error) {
                loginView.showProgressBar();
                loginUseCase.login(username, password, this);
            }
        }
    }

    @Override
    public void onDestroy() {
        loginView = null;
    }

    @Override
    public void onFailed() {
        if (loginView != null) {
            loginView.hideProgressBar();
            loginView.setLoginFailedError();
        }
    }

    @Override
    public void onSuccess() {
        if (loginView != null) {
            loginView.hideProgressBar();
            loginView.loginSuccess();
        }
    }
}
