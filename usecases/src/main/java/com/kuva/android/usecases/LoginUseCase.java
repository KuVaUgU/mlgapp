package com.kuva.android.usecases;

/**
 * Created by KuVa on 30.03.2016.
 */
public interface LoginUseCase
{
    void login(String username, String password, OnLoginFinishedListener onLoginFinishedListener);

    interface OnLoginFinishedListener {

        void onFailed();

        void onSuccess();
    }
}
