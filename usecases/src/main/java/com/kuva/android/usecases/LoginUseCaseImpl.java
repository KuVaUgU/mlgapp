package com.kuva.android.usecases;

import android.os.Handler;

/**
 * Created by KuVa on 30.03.2016.
 */
public class LoginUseCaseImpl implements LoginUseCase
{

    @Override
    public void login(final String username, final String password, final OnLoginFinishedListener onLoginFinishedListener)
    {
        new Handler().postDelayed(new Runnable() {
            @Override public void run() {

                if (username.equals("kuva") || password.equals("nostupid"))
                    onLoginFinishedListener.onSuccess();
                else
                    onLoginFinishedListener.onFailed();
            }
        }, 2000);
    }
}
